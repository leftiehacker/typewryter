$(document).ready(function() {
  // toggle post info on dashboard
  function togglePostInfo(e) {
    var target = $(e.target).closest('.post-container')
    var body = target.find('.content-body')
    var post = target.find('.content-post')

    body.toggle()
    post.toggle()
  }

  $('.post-info-toggle').click(togglePostInfo)
})
