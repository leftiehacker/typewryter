// control sidebar behavior
$(document).ready(function() {
  var sidebarToggle = $('.sidebar-toggle'),
  container = $('.container'),
  page = $(window)

  sidebarToggle.click(function() {
    container.animate({ left: (container.offset().left === 0) ? 250 : 0 }, 200)
  })

  page.resize(function() {
    if(page.width() > 600) {
      container.animate({ left: 0}, 200)
    }
  })
})
