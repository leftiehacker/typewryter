# Typewryter

Typewryter is a data-driven blogging platform built with Ruby on Rails.

## API

### Endpoint
Users' public posts are made available at the following endpoint:

`https://blmgeo-railscms.herokuapp.com/posts/json/:USERNAME`  

### Response

The response will be a JSON array of the user's public posts:

~~~
[{
  "id" : 0,
  "title" : "Title of post",
  "body" : "Body of post",
  "topic" : "Topic of post",
  "slug" : "post-slug",
  "created_at" : "2017-01-26T17:34:48.026Z",
  "updated_at" : "2017-02-05T20:52:31.824Z",
  "user_id" : 0,
  "private" : 0
}]
~~~
