Rails.application.routes.draw do
  # get 'profile/index'
  get "/profile", to: "profile#index", as: "profile"

  devise_for :users

  resources :posts do
    collection do
      get '/json/:username' => 'posts#json', as: :json
    end
    collection do
      get :public
    end
    collection do
      get :private
    end
  end

  root 'posts#index'
end
