$(function() {

var data = $('.post-data').data('posts')

// clears previous chart and loads new canvas
function renewCanvas () {
    $('.chart')
    .empty()
    .append('<canvas id="chart" width="100%" height="100%"></canvas>')
}

// draws any chart type to canvas
function createChart (type, labels, data) {
    renewCanvas()

    var ctx = document.querySelector("#chart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: type,
        data: { labels: labels, datasets: data },
        options: {
            scales: { yAxes: [{ ticks: { beginAtZero:true } }] },
            legend: { display: false }
        }
    })
}

// create topics/post chart
function displayTopics () {
    var topics = []
    var topic = [{
        label: ['Posts'],
        data: [],
        borderColor: '#5263FF',
        backgroundColor: 'rgba(82, 99, 255, 0.3)'
    }]

    data.forEach(function (post) { 
        if (!topics.includes(post.topic)) topics.push(post.topic)
    })

    topics.forEach(function (t) {
        var count = 0;
        data.forEach(function (post) {
            if (post.topic === t) count++
        })
        topic[0].data.push(count)
    })

    createChart('radar', topics, topic)
}

// create words/post chart
function displayWords () {
    var posts = []
    var words = [{
        label: ['Words'],
        data: [],
        borderColor: 'rgba(82, 99, 255, 1)',
        backgroundColor: 'rgba(82, 99, 255, 0.3)',
        borderWidth: 2
    }]

    data.forEach(function (post) { 
        if (!posts.includes(post.title)) posts.push(post.title)

        var wordCount = post.body.replace('&nbsp;', ' ').split(' ').length

        words[0].data.push(wordCount)
    })

    createChart('line', posts, words)
}

// create posts/month chart
function displayMonth () {
    var posts = []
    var translate = {
        '01': 'January',
        '02': 'February',
        '03': 'March',
        '04': 'April',
        '05': 'May',
        '06': 'June',
        '07': 'July',
        '08': 'August',
        '09': 'September',
        '10': 'October',
        '11': 'November',
        '12': 'December'
    }
    var months = [{
        label: posts,
        data: [],
        borderColor: 'rgba(82, 99, 255, 1)',
        backgroundColor: 'rgba(82, 99, 255, 0.3)',
        borderWidth: 2
    }]

    data.forEach(function (post) {
        var month = translate[post.created_at.split('-')[1]]
        if (!posts.includes(month)) posts.push(month)
    })

    posts.forEach(function (p) {
        var count = 0;
        data.forEach(function (post) {
            var month = translate[post.created_at.split('-')[1]]
            if (month === p) count++
        })
        months[0].data.push(count)
    })
    months[0].label.sort()
    months[0].data.sort()

    createChart('polarArea', posts, months)
}

// chart selection controls
$('.chart-control.topics').click(displayTopics)
$('.chart-control.words').click(displayWords)
$('.chart-control.month').click(displayMonth)


// default chart is topics by post
displayTopics()
})