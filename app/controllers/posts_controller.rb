class PostsController < ApplicationController
  before_action :find_post, only: [:edit, :update, :destroy]
  before_action :authenticate_user!, except: [:json]

  def index
    @posts = current_user.posts.order('created_at DESC')
    @username = current_user.username
  end

  def new
    @post = current_user.posts.build
  end

  def create
    @post = current_user.posts.build(post_params)
    @post.slug = @post.title.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')

    if @post.save
      redirect_to current_user.posts.last
    else
      render 'new'
    end
  end

  def show
    redirect_to root_path
  end

  def edit
  end

  def update
    if @post.update(post_params)
      redirect_to @post
    else
      render 'edit'
    end
  end

  def destroy
    @post.destroy
    redirect_to root_path
  end

  def json
    @user = User.where(:username => params[:username])

    if !@user.ids.present?
      response = {
        "status": 404,
        "success": false
      }
    else
      # only display public posts
      @posts = Post.where(:user_id => @user.ids, :private => 0)
      response = @posts.order('created_at DESC')
    end

    render json: response
  end

  def public
    @posts = current_user.posts.where(:private => 0).order('created_at DESC')
    @username = current_user.username
    render 'index'
  end

  def private
    @posts = current_user.posts.where(:private => 1).order('created_at DESC')
    @username = current_user.username
    render 'index'
  end

  private

  def find_post
    @post = Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :body, :topic, :author, :private)
  end
end
